import {Component, Input} from '@angular/core';

@Component({
	selector: 'my-header',
	templateUrl: 'app/headerComponent/headerComponent.html',
	styleUrls: ['app/headerComponent/headerComponent.css'],
})
export class HeaderComponent {
	@Input() total: number;
}
