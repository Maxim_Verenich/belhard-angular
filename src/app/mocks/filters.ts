export var FILTERS = [
	{
		group: {
			value: 'clothes',
			text: 'Одежда'
		},
		subGroups: [
			{
				value: 'trousers',
				text: 'Брюки'
			},
			{
				value: 'outerwear',
				text: 'Верхняя одежда'
			},
			{
				value: 'sweaters',
				text: 'Свитеры'
			},
			{
				value: 'jeans',
				text: 'Джинсы'
			},
			{
				value: 'homeClothes',
				text: 'Домашняя одежда'
			},
			{
				value: 'underwear',
				text: 'Нижнее белье'
			},
			{
				value: 'suits',
				text: 'Пиджаки и костюмы'
			},
			{
				value: 'shirts',
				text: 'Рубашки'
			},
			{
				value: 'tracksuits',
				text: 'Спортивные костюмы'
			},
			{
				value: 'hoodies',
				text: 'Толстовки и олимпийки'
			},
			{
				value: 'tShirts',
				text: 'Футболки и поло'
			}
		]
	},
	{
		group: {
			value: 'accessories',
			text: 'Аксессуары'
		},
		subGroups: [
			{
				value: 'wallets',
				text: 'Кошельки и визитницы'
			},
			{
				value: 'ties',
				text: 'Головные уборы'
			},
			{
				value: 'umbrella',
				text: 'Зонты'
			},
			{
				value: 'glasses',
				text: 'Очки'
			},
			{
				value: 'backpack',
				text: 'Рюкзаки'
			},
			{
				value: 'bag',
				text: 'Сумки'
			},
			{
				value: 'clock',
				text: 'Часы'
			},
			{
				value: 'scarves',
				text: 'Шарфы и платки'
			}
		]
	}
];
