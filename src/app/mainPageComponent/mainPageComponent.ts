import {Component, HostListener} from '@angular/core';
import {FILTERS} from '../mocks/filters';
import {PRODUCTS} from '../mocks/products';

@Component({
	selector: 'main-page',
	templateUrl: 'app/mainPageComponent/mainPageComponent.html',
	styleUrls: ['app/mainPageComponent/mainPageComponent.css'],
})
export class MainPageComponent {
	filters = FILTERS;
	products: any = [];
	appliedFilterName: string = '';
	groups: Object = {
		clothes: null,
		accessories: null
	};
	counts: Object = {
		clothes: 0,
		accessories: 0
	};
	total: number = 0;
	clothesSizes = ['S', 'M', 'L', 'XL', 'XXL'];
	accessoriesColors = ['#b59b7c', '#967d5f', '#a89a8b', '#574b3e', '#3b332b'];

	constructor() {
		for (let i = 0; i < FILTERS.length; i++) {
			this.groups[FILTERS[i].group.value] = [];

			for (let j = 0; j < FILTERS[i].subGroups.length; j++) {
				this.groups[FILTERS[i].group.value].push(FILTERS[i].subGroups[j].value);
			}
		}

		for (let i = 0; i < PRODUCTS.length; i++) {
			if (this.counts[PRODUCTS[i].group]) {
				this.counts[PRODUCTS[i].group]++;
			} else {
				this.counts[PRODUCTS[i].group] = 1;
			}

			for (let key in this.groups) {
				if (this.groups[key].indexOf(PRODUCTS[i].group) !== -1) {
					this.counts[key]++;
				}
			}
		}

		this.applyFilter(FILTERS[0].group);
	}

	updateTitle(filterName: string) {
		this.appliedFilterName = filterName;
	}

	applyFilter(filter: any) {
		this.updateTitle(filter.text);

		this.products = [];

		for (let i = 0; i < PRODUCTS.length; i++) {
			//apply filter for group name, not subGroup
			if (this.groups[filter.value]) {
				if (this.groups[filter.value].indexOf(PRODUCTS[i].group) !== -1) {
					this.products.push(PRODUCTS[i]);
				}
				continue;
			}

			if (PRODUCTS[i].group === filter.value) {
				this.products.push(PRODUCTS[i]);
			}
		}
	}

	getGroupNameTextByValue(groupValue: string) {
		for (let i = 0; i < this.filters.length; i++) {
			for (let j = 0; j < this.filters[i].subGroups.length; j++) {
				if (groupValue === this.filters[i].subGroups[j].value) {
					return this.filters[i].subGroups[j].text;
				}
			}
		}
	}

	@HostListener("window:scroll", []) onWindowScroll() {
		this.scrollFunction();
	}
	// When the user scrolls down 20px from the top of the document, show the button
	scrollFunction() {
		if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
			document.getElementById("scrollTop").style.opacity = '1';
		} else {
			document.getElementById("scrollTop").style.opacity = '0';
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	topFunction() {
		document.body.scrollTop = 0; // For Safari
		document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
	}
}
