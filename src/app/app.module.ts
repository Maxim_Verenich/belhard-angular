import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HeaderComponent} from './headerComponent/headerComponent';
import {MainPageComponent} from './mainPageComponent/mainPageComponent';

@NgModule({
    imports: [BrowserModule],
    declarations: [AppComponent, HeaderComponent, MainPageComponent],
    bootstrap: [AppComponent]
})
export class AppModule {
}
